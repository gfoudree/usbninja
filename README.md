Usbninja project 
===================
Security software to monitor your PC for USB removable media insertion. Provides the ability to whitelist USB drives by burning a serial to them and blocking all un-authorized devices from mounting.